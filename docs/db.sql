create table expense (
	id integer not null primary key auto_increment,
  	description varchar(100) not null,
  	type varchar(20) not null,
    value numeric(16,2) not null,
    id_creditcard integer
);

create table credit_card (
	id integer not null primary key auto_increment,
  	name varchar(100) not null,
  	active boolean default true
);
















