package com.cursosdevbase.controlefinanceiro.domain.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "credit_card")
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Boolean active;

    @Deprecated
    public CreditCard(){}

    public CreditCard(String name) {
        this.name = name;
        this.active = true;
    }
}
