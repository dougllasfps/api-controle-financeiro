package com.cursosdevbase.controlefinanceiro.domain.model;

public enum ExpenseType {
    RECURRENT,
    REGULAR,
    CREDIT_CARD
}
