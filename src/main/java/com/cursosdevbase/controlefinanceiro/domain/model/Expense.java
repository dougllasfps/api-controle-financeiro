package com.cursosdevbase.controlefinanceiro.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "expense")
public class Expense {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    @Enumerated(EnumType.STRING)
    private ExpenseType type;
    private BigDecimal value;

    @ManyToOne
    @JoinColumn(name = "id_creditcard")
    private CreditCard creditCard;

    public Expense(String description, ExpenseType type, BigDecimal value) {
        this.description = description;
        this.type = type;
        this.value = value;
    }

    @Deprecated
    public Expense(){}
}
