package com.cursosdevbase.controlefinanceiro.domain;

import lombok.Getter;

@Getter
public class ValidationError {
    private String field;
    private String message;

    public ValidationError(String name, String message) {
        this.field = name;
        this.message = message;
    }
}
