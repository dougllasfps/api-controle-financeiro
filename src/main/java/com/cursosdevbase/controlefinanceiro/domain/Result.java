package com.cursosdevbase.controlefinanceiro.domain;

import lombok.Data;

@Data
public class Result<T, E> {

    private boolean error;
    private T value;
    private E exception;

    public static <E> Result ofError(E exception) {
        var result = new Result<>();
        result.setException(exception);
        result.setError(true);
        return result;
    }

    public static <T> Result ofSucessValue(T value) {
        var result = new Result<>();
        result.setValue(value);
        return result;
    }
}
