package com.cursosdevbase.controlefinanceiro.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.List;

@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CorsConfig {

//    @Bean
//    public FilterRegistrationBean<CorsFilter> corsFilterFilterRegistrationBean(){
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", corsConfiguration());
//
//        CorsFilter corsFilter = new CorsFilter(source);
//        FilterRegistrationBean<CorsFilter> filter = new FilterRegistrationBean<>(corsFilter);
//        filter.setOrder(Ordered.HIGHEST_PRECEDENCE);
//        return filter;
//    }

    @Bean
    public CorsConfiguration corsConfiguration(){
        List<String> all = Arrays.asList("*");
        List<String> methods = Arrays.asList("GET", "PUT", "POST","DELETE","PATCH","OPTIONS","HEAD");

        CorsConfiguration corsConfiguration = new org.springframework.web.cors.CorsConfiguration();
        corsConfiguration.setAllowedOriginPatterns(all);
        corsConfiguration.setAllowedHeaders(all);
        corsConfiguration.setAllowedMethods(methods);

        return corsConfiguration;
    }
}
