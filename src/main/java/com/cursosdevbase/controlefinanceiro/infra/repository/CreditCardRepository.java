package com.cursosdevbase.controlefinanceiro.infra.repository;

import com.cursosdevbase.controlefinanceiro.domain.model.CreditCard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CreditCardRepository extends JpaRepository<CreditCard, Integer> {
    default Page<CreditCard> findActives(Pageable pageable){
        return this.findByActive(Boolean.TRUE, pageable);
    }

    Page<CreditCard> findByActive(Boolean value, Pageable pageable);

    Optional<CreditCard> findByName(String name);

    @Modifying
    @Query("update CreditCard set active =:status where id =:id ")
    void updateStatus(@Param("id") Integer id, @Param("status") Boolean status);
}
