package com.cursosdevbase.controlefinanceiro.infra.repository;

import com.cursosdevbase.controlefinanceiro.domain.model.Expense;
import com.cursosdevbase.controlefinanceiro.domain.model.ExpenseType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExpenseRepository extends JpaRepository<Expense, Integer> {
    List<Expense> findByType(ExpenseType type);
}
