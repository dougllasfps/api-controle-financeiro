package com.cursosdevbase.controlefinanceiro.application.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.validation.FieldError;

@Data
@AllArgsConstructor
public class FieldErrorDTO {

    private String field;
    private String message;

    public FieldErrorDTO(FieldError error) {
        this.field = error.getField();
        this.message = error.getDefaultMessage();
    }
}
