package com.cursosdevbase.controlefinanceiro.application.rest.creditcard.expense;

import com.cursosdevbase.controlefinanceiro.application.rest.ErrorResponse;
import com.cursosdevbase.controlefinanceiro.domain.Result;
import com.cursosdevbase.controlefinanceiro.domain.ValidationError;
import com.cursosdevbase.controlefinanceiro.domain.model.CreditCard;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("credit-cards")
@RequiredArgsConstructor
public class CreditCardController {

    private final CreditCardFormService service;

    @GetMapping
    public ResponseEntity<Page<CreditCard>> list(Pageable pageable){
        final var list = service.findAll(pageable);
        return ResponseEntity.ok(list);
    }

    @PostMapping
    public ResponseEntity<Object> save(
            @RequestBody @Valid CreditCardFormRequest request,
            BindingResult bindingResult,
            UriComponentsBuilder uriComponentsBuilder ){

        if(bindingResult.hasErrors()){
            return ResponseEntity
                    .unprocessableEntity()
                    .body(ErrorResponse.of(bindingResult).withMessage("Validation Error"));
        }

        var model = request.toModel();
        Result<CreditCard, ValidationError> result = service.executeSave(model);

        if(result.isError()){
            ValidationError error = result.getException();
            return ResponseEntity
                    .unprocessableEntity()
                    .body(ErrorResponse
                            .newInstance()
                            .withFieldError(error.getField(), error.getMessage())
                            .withMessage("Validation Error"));
        }

        URI location = uriComponentsBuilder.path("/credit-cards/{id}").buildAndExpand(model.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("{id}")
    public ResponseEntity<?> update(
            @PathVariable Integer id ,
            @RequestBody @Valid CreditCardFormRequest request,
            BindingResult bindingResult ){

        if(bindingResult.hasErrors()){
            return ResponseEntity
                    .unprocessableEntity()
                    .body(ErrorResponse.of(bindingResult).withMessage("Validation Error"));
        }

        return service.getById(id).map( creditCard -> {

            creditCard.setName(request.getName());
            Result<CreditCard, ValidationError> result = service.executeUpdate(creditCard);

            if(result.isError()){
                ValidationError error = result.getException();
                return ResponseEntity
                        .unprocessableEntity()
                        .body(ErrorResponse
                                .newInstance()
                                .withFieldError(error.getField(), error.getMessage())
                                .withMessage("Validation Error"));
            }

            return ResponseEntity.noContent().build();
        }).orElseGet(() -> ResponseEntity.notFound().build());

    }

    @GetMapping("{id}")
    public ResponseEntity findById( @PathVariable Integer id ){
        return service
                .getById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(params = "active")
    public ResponseEntity<Page<CreditCard>> listActives(@RequestParam Boolean active, Pageable pageable){
        var result = service.getListByActiveStatus(active, pageable);
        return ResponseEntity.ok(result);
    }

    @PutMapping("{id}/active")
    public ResponseEntity<?> activateOrDesactivate(@PathVariable Integer id, @RequestBody CreditCardStatusRequest request){
        return service
                .getById(id)
                .map(creditCard -> {
                    request.set(creditCard);
                    service.updateStatus(creditCard);
                    return ResponseEntity.noContent().build();
                })
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
