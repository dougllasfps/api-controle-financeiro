package com.cursosdevbase.controlefinanceiro.application.rest.creditcard.expense;

import com.cursosdevbase.controlefinanceiro.domain.model.CreditCard;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreditCardStatusRequest {

    @NotNull
    private Boolean active;

    public void set(CreditCard creditCard){
       creditCard.setActive(active);
    }

}
