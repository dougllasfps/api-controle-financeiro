package com.cursosdevbase.controlefinanceiro.application.rest.creditcard.expense;

import com.cursosdevbase.controlefinanceiro.domain.Result;
import com.cursosdevbase.controlefinanceiro.domain.ValidationError;
import com.cursosdevbase.controlefinanceiro.domain.model.CreditCard;
import com.cursosdevbase.controlefinanceiro.infra.repository.CreditCardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CreditCardFormService {

    private final CreditCardRepository repository;

    @Transactional
    public Result<CreditCard, ValidationError> executeSave(CreditCard creditCard){
        Optional<CreditCard> existentOneWithSameName = repository.findByName(creditCard.getName());

        if(existentOneWithSameName.isPresent()){
            return Result.ofError(new ValidationError("name", "Já cadastrado"));
        }

        repository.save(creditCard);
        return Result.ofSucessValue(creditCard);
    }

    @Transactional
    public Result<CreditCard, ValidationError> executeUpdate(CreditCard creditCard){
        Optional<CreditCard> existentOneWithSameName = repository.findByName(creditCard.getName());

        if(existentOneWithSameName.isPresent() && !existentOneWithSameName.get().getId().equals(creditCard.getId())){
            return Result.ofError(new ValidationError("name", "Já cadastrado"));
        }

        repository.save(creditCard);
        return Result.ofSucessValue(creditCard);
    }

    public Optional<CreditCard> getById(Integer id) {
        return repository.findById(id);
    }

    public Page<CreditCard> getListByActiveStatus(Boolean active, Pageable pageable) {
        return repository.findByActive(active, pageable);
    }

    public Page<CreditCard> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Transactional
    public void updateStatus(CreditCard creditCard) {
        repository.updateStatus(creditCard.getId(), creditCard.getActive());
    }
}
