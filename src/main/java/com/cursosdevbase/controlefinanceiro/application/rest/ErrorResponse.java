package com.cursosdevbase.controlefinanceiro.application.rest;

import lombok.Data;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class ErrorResponse {
    private String message;
    private List<FieldErrorDTO> errors;

    private ErrorResponse(){}

    public List<FieldErrorDTO> getErrors() {
        if(this.errors == null){
            this.errors = new ArrayList<>();
        }
        return errors;
    }

    public static ErrorResponse newInstance(){
        return new ErrorResponse();
    }

    public static ErrorResponse of(BindingResult bindingResult) {
        var response = new ErrorResponse();
        List<FieldErrorDTO> fieldErrors = bindingResult
                .getFieldErrors()
                .stream()
                .map(FieldErrorDTO::new)
                .collect(Collectors.toList());
        response.setErrors(fieldErrors);
        return response;
    }

    public ErrorResponse withMessage(String message){
        this.message = message;
        return this;
    }

    public ErrorResponse withFieldError(String field, String message){
        getErrors().add(new FieldErrorDTO(field, message));
        return this;
    }
}
