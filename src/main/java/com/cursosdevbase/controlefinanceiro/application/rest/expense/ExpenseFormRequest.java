package com.cursosdevbase.controlefinanceiro.application.rest.expense;

import com.cursosdevbase.controlefinanceiro.domain.model.Expense;
import com.cursosdevbase.controlefinanceiro.domain.model.ExpenseType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ExpenseFormRequest {

    @NotBlank
    private String description;
    @NotNull
    private ExpenseType type;
    @NotNull
    private BigDecimal value;

    public Expense toModel(){
        return new Expense(this.description, this.type, this.value);
    }
}
