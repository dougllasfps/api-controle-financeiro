package com.cursosdevbase.controlefinanceiro.application.rest.expense;

import com.cursosdevbase.controlefinanceiro.application.rest.ErrorResponse;
import com.cursosdevbase.controlefinanceiro.domain.model.Expense;
import com.cursosdevbase.controlefinanceiro.domain.model.ExpenseType;
import com.cursosdevbase.controlefinanceiro.infra.repository.ExpenseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/expenses")
@RequiredArgsConstructor
public class ExpenseController {

    private final ExpenseRepository repository;

    @PostMapping
    public ResponseEntity<Object> save(
            @RequestBody @Valid ExpenseFormRequest request,
            BindingResult bindingResult,
            UriComponentsBuilder uriComponentsBuilder ){

        if(bindingResult.hasErrors()){
            return ResponseEntity
                    .unprocessableEntity()
                    .body(ErrorResponse.of(bindingResult).withMessage("Validation Error"));
        }

        Expense model = request.toModel();
        repository.save(model);
        URI location = uriComponentsBuilder.path("/expenses/{id}").buildAndExpand(model.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("{id}")
    public ResponseEntity findById( @PathVariable Integer id ){
        return repository
                .findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping(params = "type")
    public ResponseEntity<Iterable<Expense>> listExpensesByType(@RequestParam ExpenseType type){
        var result = repository.findByType(type);
        return ResponseEntity.ok(result);
    }
}
