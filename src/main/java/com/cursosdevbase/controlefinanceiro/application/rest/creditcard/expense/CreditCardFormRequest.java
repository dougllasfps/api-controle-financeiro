package com.cursosdevbase.controlefinanceiro.application.rest.creditcard.expense;

import com.cursosdevbase.controlefinanceiro.domain.model.CreditCard;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreditCardFormRequest {

    @NotBlank
    private String name;

    public CreditCard toModel(){
        return new CreditCard(this.name);
    }

}
